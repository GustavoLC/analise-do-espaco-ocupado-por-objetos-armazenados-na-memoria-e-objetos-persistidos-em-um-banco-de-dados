from Config.config import *
import sys

class Tanque(db.Model):
    __tablename__ = 'Tanque'
    id = db.Column(db.Integer, primary_key=True)
    nome = db.Column(db.String(254))
    volumeLitros = db.Column(db.Integer)
    


    # método para expressar a pessoa em forma de texto
    def __str__(self):
        return f"{self.id}, {self.nome}, {self.volumeLitros}"

    def calc_tam(self, valor):
        valor += sys.getsizeof(self.id)
        valor += sys.getsizeof(self.nome)
        valor += sys.getsizeof(self.volumeLitros)
        return valor    