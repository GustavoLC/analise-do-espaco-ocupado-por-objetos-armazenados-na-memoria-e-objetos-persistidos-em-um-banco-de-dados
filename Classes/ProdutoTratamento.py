from Config.config import * 
import sys

class ProdutoTratamento(db.Model):
    __tablename__ = 'ProdutoTratamento'

    id = db.Column(db.Integer, primary_key=True)
    nome_produto = db.Column(db.String(100), nullable=False)

    def __str__(self):
        return f"{self.id}, {self.nome_produto}"
        
    def calc_tam(self, valor):
        valor += sys.getsizeof(self.id)
        valor += sys.getsizeof(self.nome_produto)
        return valor  
