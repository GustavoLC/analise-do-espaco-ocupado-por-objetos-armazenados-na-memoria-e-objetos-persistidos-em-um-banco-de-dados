# importações
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import os

# # configurações
# app = Flask(__name__) # vínculo com o Flask

# # sqlalchemy
# app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite://"
# app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # remover warnings
# db = SQLAlchemy(app) # vínculo com o SQLAlchemy

# configurações
app = Flask(__name__) # vínculo com o Flask

# caminho do arquivo de banco de dados
path = os.path.dirname(os.path.abspath(__file__))
arquivobd = os.path.join(path, 'Teste.db')

# sqlalchemy
app.config['SQLALCHEMY_DATABASE_URI'] = "sqlite:///"+arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # remover warnings
db = SQLAlchemy(app) # vínculo com o SQLAlchemy



