import os, sys

from sqlalchemy import null
currentdir = os.path.dirname(os.path.realpath(__file__)) 
parentdir = os.path.dirname(currentdir) 
sys.path.append(parentdir)

from datetime import date
from Config.config import *
from Classes.Tanque import *
from Classes.ProdutoTratamento import *
from Classes.ProdutoTratamentoTanque import *

from Config.criaTabelas import apaga_e_cria_banco

apaga_e_cria_banco()


valor = 0
contObj = 0
contAux = 3



while (contObj < 1000000000000):
    if (contObj == contAux):
        print(valor, contObj)
        text = input("Pressione enter para continuar")
        contAux = contAux * 3
    
    #-------------------Tanque-------------------

    t1 = Tanque(
        nome="Atlântico 2", 
        volumeLitros=50)

    db.session.add(t1)

    #-------------------ProdutoTratamento-------------------


    prodTratamento1 = ProdutoTratamento(
        nome_produto = "Filtro MegaHiper5200",
    )
    db.session.add(prodTratamento1)

    #-------------------ProdutoTratamentoTanque-------------------

    prodTratamentoTanque1 = ProdutoTratamentoTanque(
        tanque = t1,
        produto_tratamento = prodTratamento1,
        dosagem = 1,
        periodicidade = "Mensal"
    )
    db.session.add(prodTratamentoTanque1)
    db.session.commit()

    contObj += 3

    valor = Tanque.calc_tam(t1,valor)
    valor = ProdutoTratamentoTanque.calc_tam(prodTratamentoTanque1, valor)
    valor = ProdutoTratamento.calc_tam(prodTratamento1, valor)


print(valor)